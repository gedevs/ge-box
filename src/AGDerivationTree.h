// Tree.h -*- C++ -*-
#ifndef _AGDERIVATIONTREE_H_
#define _AGDERIVATIONTREE_H_

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* This class implements an n-ary derivation tree containing AGSymbol*.      */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

//#include "libGEdefs.h"

//#include<vector>
#include "Tree.h"
#include "AGSymbol.h"
#include "Phenotype.h"

enum TraversalReason {CLEAR,PRINT,PHENOTYPE};

using namespace std;

typedef Tree<AGSymbol*> AGTree;

class AGDerivationTree{
	public:
		AGDerivationTree(bool recycleNodes=false);
		AGDerivationTree(const AGDerivationTree &copy);
		virtual ~AGDerivationTree();
		virtual bool isRecycling() const;
		virtual void setTree(AGTree* newTree);
		virtual AGTree* getTree() const;
		virtual void clearTree();
		static void clearSubtreeBelow(AGTree& node, bool recycleNodes);
		virtual void printTree();
		virtual void loadPhenotype(Phenotype* pheno);
	protected:
		AGTree* tree;
		bool recycle;
		static void traverseTree(AGTree& currentNode, TraversalReason reason, bool recycleNodes, Phenotype* pheno=NULL);

};


#endif

