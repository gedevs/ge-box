// GEGrammar.h -*- C++ -*-
#ifndef _GEGRAMMAR_H_
#define _GEGRAMMAR_H_

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* This class implements the standard GE mapping process. It implements the  */
/* virtual methods genotype2phenotype and phenotype2genotype, and the        */
/* standard GE wrapping operator.                                            */
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

#include "libGEdefs.h"

#include<stack>
#include<vector>

#include "CFGrammar.h"
#include "AGSymbol.h"
#include "AGLookUp.h"
#include "AGDerivationTree.h"
#include "AGContext.h"

using namespace std;

class GEGrammar : public CFGrammar{
	public:
		GEGrammar(bool useAttributeGrammar=false,AGLookUp* lookup=NULL,AGContext* context=NULL);
		GEGrammar(const Genotype &,bool useAttributeGrammar=false,AGLookUp* lookup=NULL,AGContext* context=NULL);
		GEGrammar(const Phenotype &,bool useAttributeGrammar=false,AGDerivationTree* newAGDTree=NULL,AGLookUp* lookup=NULL,AGContext* context=NULL);
		GEGrammar(const GEGrammar&);
		virtual ~GEGrammar();
		unsigned int getMaxWraps() const;
		void setMaxWraps(const unsigned int);
		bool getUseAG() const;
		const DerivationTree* getDerivationTree();
		const AGDerivationTree* getAGDerivationTree() const;
		const vector<Production*>* getProductions();                
		const AGLookUp* getAGLookUp() const;
		const AGContext* getAGContext() const;
	protected:
		vector<Production*> productions;
		bool genotype2Phenotype();
		bool phenotype2Genotype();
		void buildDTree(DerivationTree&, vector<Production*>::iterator&);
		virtual AGMapState buildAGTree(bool init,AGTree &currentNode, const Symbol* nodeSym, AGContext& context, stack<const Symbol*>& nonterminals,
        		Genotype::iterator& genoIt, unsigned int& newEffectiveSize, unsigned int& wraps, bool& gotToUseWrap, int prevProdIndex, unsigned int backtrack,
				const bool growMethod=false,const unsigned int maxDepth=0, int genoLenSoFar=0);
		virtual AGMapState getAGInitProductionSet(Rule* &rulePtr, AGTree& currentAGNode, const unsigned int maxDepth, const bool growMethod, vector<int> &possibleRules);
                int chooseProduction(int codon, Rule *rulePtr, AGTree* =NULL);
		virtual Symbol* createSymbol(const Symbol&, bool =false);
                bool useAG;
                AGLookUp* agLookUp;  
		AGContext* agContext;
		AGDerivationTree* agDtree;
	private:
		bool genotype2Phenotype(const bool);
		int genotype2PhenotypeStep(stack<const Symbol*>&,Genotype::iterator,bool, Rule* &, int&, AGTree* =NULL);		        
		bool geno2phenoStepDecision(int result, Genotype::iterator& genoIt, unsigned int& newEffectiveSize, bool& gotToUseWrap, unsigned int& wraps);
		unsigned int maxWraps;
};

#endif

