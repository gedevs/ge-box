// GEListGenome.cpp -*- C++ -*-

#ifndef _GELISTGENOME_CPP
#define _GELISTGENOME_CPP

#include"GEListGenome.h"

using namespace std;

int GEListGenome::getEffectiveSize() const{
	return effSize;
}
void GEListGenome::setEffectiveSize(const int newEffSize){
	effSize=newEffSize;
}

// Allow crossover only on effective length of individuals
int GEListGenome::effCrossover(const GAGenome &p1, const GAGenome &p2, GAGenome *c1, GAGenome *c2){
  
	const GEListGenome &mom=static_cast<const GEListGenome &>(p1);
	const GEListGenome &dad=static_cast<const GEListGenome &>(p2);
	int nc=0;
  // Crossover points
	int a,b;
  
	if(mom.getEffectiveSize()>mom.size()){
		// Wrapping was used, choose random location
		a = GARandomInt(0, mom.size());
	}
	else{ // Choose point from effective length
		a = GARandomInt(0, mom.getEffectiveSize());
	}
	if(dad.getEffectiveSize()>dad.size()){
		// Wrapping was used, choose random location
		b = GARandomInt(0,dad.size());
	}
	else{ // Choose point from effective length
  		b = GARandomInt(0, dad.getEffectiveSize());
	}
	
	GAList<unsigned char> * list;
	
  if(c1){
		GAListGenome<unsigned char> &sis=
			static_cast<GAListGenome<unsigned char> &>(*c1);
		sis.GAList<unsigned char>::copy(mom);
		list = dad.GAList<unsigned char>::clone(b);
		if(a < mom.size()){
			unsigned char *site = sis.warp(a);
			while(sis.tail() != site){
        // delete the tail node
				sis.destroy();		
			}
			// trash the trailing node (list[a])
			sis.destroy();		
		}
		else{ // move to the end of the list
			sis.tail();		
		}
		// stick the clone onto the end
		sis.insert(list);		
		delete list;
    // set iterator to head of list
		sis.head();			
		nc += 1;
	}
	
	if(c2){
		GAListGenome<unsigned char> &bro=
			static_cast<GAListGenome<unsigned char> &>(*c2);
		bro.GAList<unsigned char>::copy(dad);
		list = mom.GAList<unsigned char>::clone(a);
		if(b < dad.size()){
			unsigned char *site = bro.warp(b);
      
			while(bro.tail() != site){
        // delete the tail node
				bro.destroy();		
			}
			// trash the trailing node (list[a])
			bro.destroy();		
		}
		else{ // move to the end of the list
			bro.tail();		
		}
		// stick the clone onto the end
		bro.insert(list);		
		delete list;
    // set iterator to head of list
		bro.head();			
		nc += 1;
	}
	return nc;
}

//Mutate each bit of genome with probability pmut
int GEListGenome::pointMutator(GAGenome & g, float pmut){
	GAListGenome<unsigned char> &genome=
		static_cast<GAListGenome<unsigned char> &>(g);
	register unsigned char mutator;
	register int nMut = 0;
	unsigned int n = genome.size();
	unsigned int bits = sizeof(unsigned char)*8;

	//Don't mutate if prob. mut. is <= 0
	if(pmut <= 0.0) return 0;
	//Go through each codon
	for(unsigned int ii=0;ii<n;ii++){
		mutator=0;
		//Go through each bit
		for(unsigned int jj=0;jj<bits;jj++){
			mutator=mutator<<1;
			//Flip the bit with prob. pmut
			if(GAFlipCoin(pmut)){
				mutator++;
				nMut++;
			}
		}
		//Update the genome
		*genome[ii]=*genome[ii]^mutator;
	}
	//Return number of mutations made
	return nMut;
}

#endif

