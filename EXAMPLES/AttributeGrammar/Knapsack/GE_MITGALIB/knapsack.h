//knapsack.h -*- C++ -*-
#ifndef _KNAPSACK_H_
#define _KNAPSACK_H_

using namespace std;

/************************************************************
// Problem Description Begins.
*************************************************************/ 

//Problem Source: 1st problem at http://people.brunel.ac.uk/~mastjjb/jeb/orlib/files/mknap1.txt
//File format to decode mkap1.txt is described at http://people.brunel.ac.uk/~mastjjb/jeb/orlib/mknapinfo.html
//no of items=6, number of knapsacks/constraints=10, optimal value=3800
const unsigned int KSITEMS=6, KSKNAPSACKS=10;
const unsigned int KSENDITEM=KSITEMS; //A special item to terminate mapping. Does not contribute to weights or profits.
//profits
const int KSprofits[KSITEMS]={100, 600, 1200, 2400, 500, 2000};
const int KSweights[][KSITEMS]={        {8, 12, 13, 64, 22, 41},
                                        {8, 12, 13, 75, 22, 41},
                                        {3, 6, 4, 18, 6, 4},  
                                        {5, 10, 8, 32, 6, 12},
                                        {5, 13, 8, 42, 6, 20},
                                        {5, 13, 8, 48, 6, 20},   
                                        {0, 0, 0, 0, 8, 0},
                                        {3, 0, 4, 0, 8, 0},
                                        {3, 2, 4, 0, 8, 4},
                                        {3, 2, 4, 8, 8, 4}
                                };
const unsigned int KSconstraints[KSKNAPSACKS]={80, 96, 20, 36, 44, 48, 10, 18, 22, 24};

/************************************************************
// Helper Function
*************************************************************/
//Function prototype: This function generates a vector of
//items allowed at the specified state of derivation process.
//The state is reflected by a vector of previously used items.

const vector<int>* genAllowedItems(const vector<bool>* k_used);
        

#endif
