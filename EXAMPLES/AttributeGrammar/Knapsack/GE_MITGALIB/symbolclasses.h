// symbolclasses.h -*- C++ -*-
#ifndef _SYMBOLCLASSES_H_
#define _SYMBOLCLASSES_H_

#include <iostream>
#include <string>
#include<GE/ge.h>

using namespace std;


/************************************************************
// Grammar Specific Classes Begin. 
*************************************************************/

//////////////////////////////////////////////////////////////////////////////
// This class implements the symbols item(n); where 0<=n<KSITEMS. Since each
// item is only different in terms of its index, the same class can be used 
// for all the items.  
class Item : public AGSymbol{
        public:
                Item(const string, const SymbolType);
                Item(const Item&);
                ~Item();
                virtual AGMapState updateSynthesisedAttributes(AGContext &, const int prodIndex, AGLookUp& lookUp);
		virtual AGMapState updateInheritedAttributes(AGContext &, const int prodIndex, AGLookUp& lookUp);
                unsigned int getItemNo() const;
//                bool operator==(const Item&);
                Item &operator=(const Item);
        protected: 
                unsigned int itemNo;       //This is enough to point to the relevant weights. 
};


//////////////////////////////////////////////////////////////////////////////
// This class implements the symbol 'I' from the grammar. 

class I : public AGSymbol{
        public:
                I(const string, const SymbolType,const unsigned int =0);
                I(const I&);
                ~I();
                virtual AGMapState updateSynthesisedAttributes(AGContext &, const int prodIndex, AGLookUp& lookUp);
		virtual AGMapState updateInheritedAttributes(AGContext &, const int prodIndex, AGLookUp& lookUp);
                const unsigned int getUsedItemNo() const;
		//Override this method to implement forward checking.
		virtual const vector<bool>* getValidProductionIndices() const;
                I &operator=(const I);
//                bool operator==(const I&);
        protected: 
                unsigned int s_usedItemNo;            
		//This variable is declared to confirm forward checking with the getValidProductionIndices()
		//Initialise it in the constructor.
		vector<bool> i_validProductionIndices; 
};

//////////////////////////////////////////////////////////////////////////////
// This class implements the symbol 'K' from the grammar. 

class K : public AGSymbol{
        public:
                K(const string, const SymbolType,const unsigned int =0);
                K(const K&);
                ~K();
                virtual AGMapState updateSynthesisedAttributes(AGContext &, const int prodIndex, AGLookUp& lookUp);
		virtual AGMapState updateInheritedAttributes(AGContext &, const int prodIndex, AGLookUp& lookUp);
		//Override this method to implement forward checking.
		virtual const vector<bool>* getValidProductionIndices() const;
		virtual const vector<bool>* getUsedItems() const;
                K &operator=(const K);
        protected: 
		//This variable is declared to confirm forward checking with the getValidProductionIndices()
		//Initialise it in the constructor.
		vector<bool> i_validProductionIndices; 
		vector<bool> i_usedItems;
		
};

//////////////////////////////////////////////////////////////////////////////
// This class implements the symbol 'S' from the grammar. 
// This class does not need to implement getValidProductionIndices because
// it only represents the start symbol for which, in this problem, all 
// productions are valid. The updateSynthesise/InheritedAttribute() methods
// are also empty because it does not inherit or synthesise anything. 

class S : public AGSymbol{
        public:
                S(const string, const SymbolType,const unsigned int =0);
                S(const S&);
                ~S();
                virtual AGMapState updateSynthesisedAttributes(AGContext &, const int prodIndex, AGLookUp& lookUp);
		virtual AGMapState updateInheritedAttributes(AGContext &, const int prodIndex, AGLookUp& lookUp);		
		virtual const vector<bool>* getAllowedItems() const;
                S &operator=(const S);
        protected: 
		//This variable is declared to confirm forward checking with the getValidProductionIndices()
		//Initialise it in the constructor.
		vector<bool> i_allowedItems;		
};




#endif
